package com.altacode.sololearntask

import android.app.Application
import android.arch.persistence.room.Room
import android.support.multidex.MultiDex
import com.altacode.sololearntask.notification.NotificationHelper
import com.altacode.sololearntask.repository.FeedRepository
import com.altacode.sololearntask.repository.db.AppDatabase
import com.altacode.sololearntask.repository.network.RetrofitClient
import com.altacode.sololearntask.viewmodel.FeedListViewModel
import com.facebook.common.internal.Suppliers
import com.facebook.drawee.backends.pipeline.DraweeConfig
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import okhttp3.OkHttpClient
import timber.log.Timber

class SoloLearnApplication : Application() {

    companion object {
        lateinit var instance: SoloLearnApplication
            private set

        private lateinit var feedRepository: FeedRepository
        private lateinit var feedItemsListViewModel: FeedListViewModel
        private lateinit var appDatabase: AppDatabase
        private lateinit var notificationHelper: NotificationHelper

        fun injectFeedViewModel() = feedItemsListViewModel

        fun injectFeedDao() = appDatabase.feedItemsDao()
        fun injectPinDao() = appDatabase.pinItemsDao()
        fun injectNotificationHelper() = notificationHelper
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this);
        instance = this
        val okHttpClient = OkHttpClient.Builder().build()
        val imagePipelineConfig = OkHttpImagePipelineConfigFactory.newBuilder(this, okHttpClient)
                .experiment()
                .setBitmapPrepareToDraw(true, 0, Integer.MAX_VALUE, true)
                .experiment()
                .setSmartResizingEnabled(Suppliers.BOOLEAN_TRUE)
                .build()

        val draweeConfig = DraweeConfig.newBuilder()
                .setDrawDebugOverlay(false)
                .build()


        Fresco.initialize(this, imagePipelineConfig, draweeConfig)

        Timber.uprootAll()
        Timber.plant(Timber.DebugTree())


        appDatabase = Room.databaseBuilder(applicationContext,
                AppDatabase::class.java, "app-database").fallbackToDestructiveMigration().build()

        feedRepository = FeedRepository(RetrofitClient.getFeedApi(), appDatabase.feedItemsDao())
        feedItemsListViewModel = FeedListViewModel(feedRepository)


        notificationHelper = NotificationHelper(this)

    }

}
