package com.altacode.sololearntask.viewmodel

import com.altacode.sololearntask.repository.FeedRepository
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.utils.concatenateParams
import io.reactivex.Observable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class FeedListViewModel(private val feedRepository: FeedRepository) {

    fun getFeedData(page: Int, vararg fields: String): Observable<List<FeedItem>> {
        //Create the data for your UI, the feedItems lists and maybe some additional data needed as well
        return feedRepository.getFeedData(page, concatenateParams(fields))
                .debounce(1000, TimeUnit.MILLISECONDS) // wait 1000 ms before showing db result
                .map {
                    Timber.d("Mapping feedItems to UIData...")
                    it
                }
    }


}
