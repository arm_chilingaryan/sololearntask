package com.altacode.sololearntask.utils

import android.widget.Toast
import com.altacode.sololearntask.SoloLearnApplication

fun shortToast(message: String) = Toast.makeText(SoloLearnApplication.instance, message, Toast.LENGTH_SHORT).show()

fun concatenateParams(fields: Array<out String>): String {
    var queryField = StringBuilder("")
    for (field in fields) {
        queryField.append(field).append(",")
    }

    if (!queryField.isEmpty()) {
        queryField.deleteCharAt(queryField.length - 1)
    }
    return queryField.toString()
}