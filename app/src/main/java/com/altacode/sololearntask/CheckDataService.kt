package com.altacode.sololearntask

import com.altacode.sololearntask.notification.NotificationHelper
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.network.RestConstants
import com.altacode.sololearntask.repository.network.RetrofitClient
import com.altacode.sololearntask.utils.concatenateParams
import com.altacode.sololearntask.utils.shortToast
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class CheckDataService : JobService() {
    private var notificationHelper = SoloLearnApplication.injectNotificationHelper()

    override fun onStartJob(job: JobParameters): Boolean {
        doChecking(job)
        return true
    }

    override fun onStopJob(job: JobParameters): Boolean {
        return true
    }


    private fun doChecking(job: JobParameters) {
        var observableFeed = getFeedObservable(arrayOf(RestConstants.HEADLINE, RestConstants.THUMBNAIL, RestConstants.TRIAL_TEXT))
        var observableDB = getDBObservable()

        doParallelCall(observableFeed, observableDB, job)

    }

    private fun doParallelCall(observableFeed: Observable<List<FeedItem>>, observableDB: Observable<FeedItem>, job: JobParameters) {
        Observable.zip(observableFeed, observableDB, BiFunction<List<FeedItem>, FeedItem, Boolean> { t1, t2 ->
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.CANADA)
            val dateDB = format.parse(t2.webPublicationDate)
            val dateFeed = format.parse(t1.get(0).webPublicationDate)
            dateFeed.after(dateDB)
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    println(it)
                    if (it) {
                        // generate Notification
                        notificationHelper.notify(NotificationHelper.NOTIFICATION_ID, notificationHelper.getNotification("News Feed ", "Checkout updated news"))
                    }
                    jobFinished(job, false)
                }, {
                    println(it)
                    jobFinished(job, true)
                })
    }

    private fun getFeedObservable(fields: Array<out String>): Observable<List<FeedItem>> {
        return RetrofitClient.getFeedApi().getData(concatenateParams(fields), 1).map {
            it.response.results
        }
    }

    private fun getDBObservable(): Observable<FeedItem> {
        return SoloLearnApplication.injectFeedDao().getFeedFirstItem().toObservable()
    }
}