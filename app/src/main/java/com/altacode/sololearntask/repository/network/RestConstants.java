package com.altacode.sololearntask.repository.network;

/**
 * Created by Chilingaryan on 6/8/18.
 */

public class RestConstants {

    public static final String THUMBNAIL = "thumbnail";
    public static final String HEADLINE = "headline";
    public static final String TRIAL_TEXT = "trailText";

}
