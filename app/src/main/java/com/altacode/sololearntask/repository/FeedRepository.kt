package com.altacode.sololearntask.repository

import com.altacode.sololearntask.repository.db.FeedItemsDao
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.network.FeedApi
import com.altacode.sololearntask.utils.shortToast
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class FeedRepository(val feedApi: FeedApi, val feedDao: FeedItemsDao) {

    fun getFeedData(page: Int, params: String): Observable<List<FeedItem>> {
        return Observable.concat(
                getFeedDataFromDb(),
                getFeedDataFromApi(page, params)
                        .onErrorResumeNext(Observable.empty())) // do nothing, this means we have no internet connection
    }


    private fun getFeedDataFromDb(): Observable<List<FeedItem>> {
        return feedDao.getFeedData().filter {
            it.isNotEmpty()
        }
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext {
                    Timber.d("Dispatching ${it.size} feedItems from DB...")
                }
    }

    private fun getFeedDataFromApi(page: Int, params: String): Observable<List<FeedItem>> {
        return feedApi.getData(params, page)
                .map {
                    it.response.results
                }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext {
                    Timber.d("Dispatching ${it.size} feedItems from API...")
                    storeFeedDataInDb(it)
                }

    }

    fun storeFeedDataInDb(users: List<FeedItem>) {
        Observable.fromCallable { feedDao.insertAll(users) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe {
                    Timber.d("Inserted ${users.size} feedItems from API in DB...")
                }
    }


}
