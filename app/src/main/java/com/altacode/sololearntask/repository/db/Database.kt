package com.altacode.sololearntask.repository.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.models.PinDBO

@Database(entities = [FeedItem::class,PinDBO::class], version = 1,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun feedItemsDao(): FeedItemsDao
    abstract fun pinItemsDao(): PinItemsDao
}