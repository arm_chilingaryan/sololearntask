package com.altacode.sololearntask.repository.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "pinedItemsTable")
data class PinDBO(
        @PrimaryKey
        val id: String, //sport/live/2018/jun/09/south-africa-v-england-rugby-union-first-test-live
        var isPinned: Boolean?, //liveblog
        val type: String?, //liveblog
        val sectionId: String?, //sport
        val sectionName: String?, //Sport
        val webPublicationDate: String?, //2018-06-09T17:30:27Z
        val webTitle: String?, //South Africa 42-39 England: rugby union first Test – as it happened
        val webUrl: String?, //https://www.theguardian.com/sport/live/2018/jun/09/south-africa-v-england-rugby-union-first-test-live
        val apiUrl: String?, //https://content.guardianapis.com/sport/live/2018/jun/09/south-africa-v-england-rugby-union-first-test-live
        @Embedded
        var fields: Fields,
        val isHosted: Boolean?, //false
        val pillarId: String?, //pillar/sport
        val pillarName: String?//Sport
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Fields::class.java.classLoader),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun equals(o: Any?): Boolean {

        var sameSame = false

        if (o != null && o is FeedItem) {
            sameSame = this.id == o.id
        }

        return sameSame
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    @Entity
    data class Fields(
            @PrimaryKey
            val headline: String?, //South Africa 42-39 England: rugby union first Test – as it happened
            val thumbnail: String?, //https://media.guim.co.uk/4cd8fe9d292d8291a33c9f0d441064e67fae4c3a/0_0_3500_2100/500.jpg
            val trailText: String?//https://media.guim.co.uk/4cd8fe9d292d8291a33c9f0d441064e67fae4c3a/0_0_3500_2100/500.jpg
    ) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString()) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(headline)
            parcel.writeString(thumbnail)
            parcel.writeString(trailText)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Fields> {
            override fun createFromParcel(parcel: Parcel): Fields {
                return Fields(parcel)
            }

            override fun newArray(size: Int): Array<Fields?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeValue(isPinned)
        parcel.writeString(type)
        parcel.writeString(sectionId)
        parcel.writeString(sectionName)
        parcel.writeString(webPublicationDate)
        parcel.writeString(webTitle)
        parcel.writeString(webUrl)
        parcel.writeString(apiUrl)
        parcel.writeParcelable(fields, flags)
        parcel.writeValue(isHosted)
        parcel.writeString(pillarId)
        parcel.writeString(pillarName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PinDBO> {
        override fun createFromParcel(parcel: Parcel): PinDBO {
            return PinDBO(parcel)
        }

        override fun newArray(size: Int): Array<PinDBO?> {
            return arrayOfNulls(size)
        }
    }
}


