package com.altacode.sololearntask.repository.network;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String TAG = RetrofitClient.class.getSimpleName();
    private static String baseUrl = "https://content.guardianapis.com/";
    private static volatile Retrofit sRetrofit = null;
    private static FeedApi feedApiService;

    public RetrofitClient() {
    }

    public static FeedApi getFeedApi() {
        return initFeedApi();
    }

    private static FeedApi initFeedApi() {
        if (feedApiService == null) {
            synchronized (RetrofitClient.class) {
                if (feedApiService == null) {
                    feedApiService = getRetrofit().create(FeedApi.class);
                }
            }
        }
        return feedApiService;
    }

    @RequiresPermission(Manifest.permission.INTERNET)
    private synchronized static Retrofit getRetrofit() {
        if (sRetrofit == null) {
            synchronized (RetrofitClient.class) {
                if (sRetrofit == null) {
                    sRetrofit = new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .client(createClient())
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .build();
                }
            }
        }
        return sRetrofit;
    }

    private static OkHttpClient createClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> {
            if (message.startsWith("{")) {
                int spacesToIndentEachLevel = 2;
                try {
                    Log.d("RETROFIT", new JSONObject(message).toString(spacesToIndentEachLevel));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("RETROFIT", message);
            }

        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new Builder()
//                .cache(new Cache(new File(SoloApplication.getInstance().getCacheDir(), "http"), 1024 * 1024 * 10))
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new ApiKeyInterceptor())
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }


    private static class ApiKeyInterceptor implements Interceptor {

        @Override
        public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
            Request original = chain.request();

            HttpUrl originalHttpUrl = original.url();

            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api-key", "0ba8fa41-aaa1-429b-805b-ee1540838d70")
                    .build();

            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder().url(url);
            Response proceed = chain.proceed(requestBuilder.build());
            return proceed;

        }
    }


}
