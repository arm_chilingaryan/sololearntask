package com.altacode.sololearntask.repository.network

import com.altacode.sololearntask.repository.models.FeedResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface FeedApi {
    @GET("search")
    fun getData(@Query("show-fields") fields: String, @Query("page") page: Int = 1): Observable<FeedResponse>
}
