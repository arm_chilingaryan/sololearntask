package com.altacode.sololearntask.repository.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.altacode.sololearntask.repository.models.FeedItem
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface FeedItemsDao {

    @Query("SELECT * FROM feedItemsTable")
    fun getFeedData(): Single<List<FeedItem>>

    @Query("SELECT * FROM feedItemsTable Order by webPublicationDate Desc LIMIT 1")
    fun getFeedFirstItem(): Single<FeedItem>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(feedItem: FeedItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(feedItems: List<FeedItem?>)
}