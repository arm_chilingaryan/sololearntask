package com.altacode.sololearntask.repository.db

import android.arch.persistence.room.*
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.models.PinDBO
import io.reactivex.Single

@Dao
interface PinItemsDao {

    @Query("SELECT * FROM pinedItemsTable")
    fun getPinData(): Single<List<PinDBO>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pinItem: PinDBO)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(pinItems: List<PinDBO?>)

    @Query("DELETE FROM pinedItemsTable WHERE id = :id")
    fun deleteItem(id: String)
}