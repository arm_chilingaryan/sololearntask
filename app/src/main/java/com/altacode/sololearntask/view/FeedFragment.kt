package com.altacode.sololearntask.view


import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.transition.Fade
import android.view.*
import com.altacode.sololearntask.*
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.models.PinDBO
import com.altacode.sololearntask.repository.network.RestConstants
import com.altacode.sololearntask.utils.DetailsTransition
import com.altacode.sololearntask.utils.EndlessRecyclerViewScrollListener
import com.altacode.sololearntask.utils.shortToast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_feed.view.*
import timber.log.Timber
import java.util.*
import kotlin.collections.LinkedHashSet


class FeedFragment : BaseFragment() {
    private var feedItemsList = LinkedHashSet<FeedItem>()
    private var pinItemsList = LinkedHashSet<PinDBO>()
    private lateinit var feedRecyclerView: RecyclerView
    private lateinit var pinRecyclerView: RecyclerView
    private var pinAdapter: PinRecyclerViewAdapter? = null
    private var feedAdapter: FeedRecyclerViewAdapter? = null
    private lateinit var pinLinearLayoutManager: LinearLayoutManager
    private lateinit var gridLayoutManager: StaggeredGridLayoutManager
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var mHandler = Handler()
    private var mRunnable = Runnable {
        if (hasConnection) {
            getFeed(1, true);
        }
        startRefresh()
    }
    var hasConnection: Boolean = false

    override fun onNetworkStateChanged(connected: Boolean) {
        hasConnection = connected
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_feed, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFeedRecyclerView(view)
        setupPinRecyclerView(view)
        getFeed(1, false)
    }

    private fun getFeed(page: Int = 1, checkUpdates: Boolean = false) {
        val feedRequest = SoloLearnApplication.injectFeedViewModel().getFeedData(page, RestConstants.HEADLINE, RestConstants.THUMBNAIL, RestConstants.TRIAL_TEXT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (checkUpdates) {
                        checkAndUpdateItems(it as ArrayList, feedItemsList)
                    } else {
                        if (page == 1) {
                            feedAdapter?.data?.clear()
                            feedAdapter?.data?.addAll(it as ArrayList<FeedItem>)
                        } else {
                            feedAdapter?.data?.addAll(it)
                        }
                        feedAdapter?.notifyDataSetChanged()
                    }
                }, {
                    shortToast("something went wrong :(")
                    Timber.e(it)
                })

        subscribe(feedRequest)
    }

    private fun checkAndUpdateItems(newFeedItems: ArrayList<FeedItem>?, feedItemsList: LinkedHashSet<FeedItem>) {
        if (!feedItemsList.contains(newFeedItems?.get(0))) {
            newFeedItems?.let {
                feedItemsList.addAll(it)
                feedAdapter?.notifyDataSetChanged()
                println("Last news has been added to List")
            }
        }
    }


    private fun setupFeedRecyclerView(view: View) {
        feedRecyclerView = view.feedRecyclerView

        gridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        linearLayoutManager = LinearLayoutManager(context)


        val style = if (feedAdapter == null)
            FeedRecyclerViewAdapter.Style.Linear // default value
        else
            feedAdapter?.style!! // previous value after returning from back stack

        feedAdapter = FeedRecyclerViewAdapter(feedItemsList, object : FeedRecyclerViewAdapter.OnItemClick {
            override fun onItemClicked(feedItem: FeedItem, view: View) {
                pinItemsList.forEach {
                    if (feedItem.id.equals(it.id)) {
                        openFragmentWithSharedTransition(it, view)
                        return
                    }
                }
                openFragmentWithSharedTransition(feedItem, view)
            }
        })
        setLayoutManagerStyle(style)

        val loadMoreListener = object : EndlessRecyclerViewScrollListener() {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                getFeed(page)
            }
        }
        feedRecyclerView.addOnScrollListener(loadMoreListener)
        feedRecyclerView.isNestedScrollingEnabled = false
    }

    private fun setupPinRecyclerView(view: View) {
        pinRecyclerView = view.pinRecyclerView

        pinLinearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        pinAdapter = PinRecyclerViewAdapter(pinItemsList, object : PinRecyclerViewAdapter.OnItemClick {
            override fun onItemClicked(pinItem: PinDBO, view: View) {
                openFragmentWithSharedTransition(pinItem, view)
            }
        })

        pinRecyclerView.adapter = pinAdapter
        pinRecyclerView.layoutManager = pinLinearLayoutManager
    }

    private fun openFragmentWithSharedTransition(item: Any, view: View) {
        val details = DetailsFragment()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            details.sharedElementEnterTransition = DetailsTransition()
            details.enterTransition = Fade()
            details.exitTransition = Fade()
            details.sharedElementReturnTransition = DetailsTransition()
        }

        var bundle = Bundle()
        if (item is PinDBO) {
            bundle.putParcelable("Item", item)
        } else {
            bundle.putParcelable("Item", item as FeedItem)
        }
        details.arguments = bundle

        activity?.supportFragmentManager
                ?.beginTransaction()
                ?.addSharedElement(view, "feed_image_transition")
                ?.replace(R.id.fragment_container, details)
                ?.addToBackStack(null)
                ?.commit()

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.view_type, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_list -> {
                if (feedAdapter?.style == FeedRecyclerViewAdapter.Style.Grid) {
                    setLayoutManagerStyle(FeedRecyclerViewAdapter.Style.Linear)
                }
                return true
            }

            R.id.menu_staggered -> {
                if (feedAdapter?.style == FeedRecyclerViewAdapter.Style.Linear) {
                    setLayoutManagerStyle(FeedRecyclerViewAdapter.Style.Grid)
                }
                return true
            }
        }
        return false
    }

    private fun setLayoutManagerStyle(style: FeedRecyclerViewAdapter.Style) {

        when (style) {
            FeedRecyclerViewAdapter.Style.Linear -> {
                feedAdapter?.style = FeedRecyclerViewAdapter.Style.Linear
                feedRecyclerView.layoutManager = linearLayoutManager
            }

            FeedRecyclerViewAdapter.Style.Grid -> {
                feedAdapter?.style = FeedRecyclerViewAdapter.Style.Grid
                feedRecyclerView.layoutManager = gridLayoutManager
            }
        }



        feedRecyclerView.adapter = feedAdapter
    }

    private fun getPinItemsFromDb() {
        SoloLearnApplication.injectPinDao().getPinData()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    pinItemsList.clear()
                    pinItemsList.addAll(it)
                    if (pinItemsList.size == 0) {
                        pinRecyclerView.visibility = View.GONE
                    } else {
                        pinRecyclerView.visibility = View.VISIBLE
                    }
                    pinAdapter?.notifyDataSetChanged()
                })

    }

    private fun startRefresh() {
        mHandler.postDelayed(mRunnable, 30 * 1000)
        println("FeedFragment.startRefresh")
    }

    private fun stopRefresh() {
        println("FeedFragment.stopRefresh")
        try {
            mHandler.removeCallbacks(mRunnable)
        } catch (ignored: Exception) {
        }
    }


    override fun onResume() {
        super.onResume()
        startRefresh()
        getPinItemsFromDb()
        (activity as MainActivity).configureSupportBar(false,"Feed")
    }

    override fun onPause() {
        super.onPause()
        stopRefresh()
    }

}

