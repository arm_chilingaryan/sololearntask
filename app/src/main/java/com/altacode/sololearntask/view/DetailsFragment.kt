package com.altacode.sololearntask.view


import android.os.Bundle
import android.os.Parcel
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.altacode.sololearntask.MainActivity

import com.altacode.sololearntask.R
import com.altacode.sololearntask.SoloLearnApplication
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.models.PinDBO
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.fragment_details.view.*
import timber.log.Timber

class DetailsFragment : Fragment() {
    var feedItem: FeedItem? = null
    var pinItem: PinDBO? = null
    var thumbnailURL: String? = null
    var headlineText: String? = null
    var trailText: String? = null
    var isPinned: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            feedItem = arguments?.getParcelable("Item")
            thumbnailURL = feedItem?.fields?.thumbnail
            headlineText = feedItem?.fields?.headline
            trailText = feedItem?.fields?.trailText
            isPinned = false
        } catch (e: Exception) {

            try {
                pinItem = arguments?.getParcelable("Item")
                thumbnailURL = pinItem?.fields?.thumbnail
                headlineText = pinItem?.fields?.headline
                trailText = pinItem?.fields?.trailText
                isPinned = pinItem?.isPinned!!
            } catch (e: Exception) {
                thumbnailURL = ""
                headlineText = ""
                trailText = ""
                isPinned = false
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var rootView = inflater.inflate(R.layout.fragment_details, container, false)
        rootView.image.setImageURI(thumbnailURL)
        rootView.header.text = headlineText
        rootView.description.text = trailText
        rootView.pin_image.isSelected = isPinned
        rootView.pin_image.setOnClickListener({
            it.isSelected = !it.isSelected
        })

        return rootView
    }

    fun storeItemToDb(item: PinDBO) {
        item.isPinned = true
        SoloLearnApplication.injectPinDao()
        Observable.fromCallable { SoloLearnApplication.injectPinDao().insert(item) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe {
                    Timber.d("Inserted ${item} feedItems from API in DB...")
                }
    }

    fun deleteItemByID(id: String) {
        SoloLearnApplication.injectPinDao()
        Observable.fromCallable { SoloLearnApplication.injectPinDao().deleteItem(id) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe {
                    Timber.d("DELETED ${id} from API in DB...")
                }
    }

    override fun onStop() {
        super.onStop()
        if (pin_image.isSelected) {
            if (pinItem != null) {
                pinItem?.let { storeItemToDb(it) }
            } else {
                var feedItemString = Gson().toJson(feedItem)
                var tempPinItem: PinDBO = Gson().fromJson<PinDBO>(feedItemString, PinDBO::class.java)
                tempPinItem.let { storeItemToDb(it) }
            }
        } else {
            if (pinItem?.id.isNullOrEmpty()) {
                feedItem?.id?.let { deleteItemByID(it) }
            } else{
                pinItem?.id?.let { deleteItemByID(it) }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).configureSupportBar(true,"Details")
    }


}
