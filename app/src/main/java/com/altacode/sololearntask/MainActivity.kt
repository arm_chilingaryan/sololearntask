package com.altacode.sololearntask


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.altacode.sololearntask.notification.NotificationHelper
import com.altacode.sololearntask.view.FeedFragment
import com.firebase.jobdispatcher.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, FeedFragment()).commit()
        }

        var cancelNotification = intent.getBooleanExtra(NotificationHelper.CANCEL_EXTRA, false)
        if (cancelNotification) {
            SoloLearnApplication.injectNotificationHelper().manager.cancel(NotificationHelper.NOTIFICATION_ID)
        }

        // Create a new dispatcher using the Google Play driver.
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))

        val myJob = dispatcher.newJobBuilder()
                .setService(CheckDataService::class.java)
                .setTag("CheckDataServiceTag")
                .setRecurring(true)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setTrigger(Trigger.executionWindow(90, 100))
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build()

        dispatcher.cancelAll()
        dispatcher.mustSchedule(myJob)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun configureSupportBar(showBackButton: Boolean, titleText: String) {
        supportActionBar?.let {
            it.setTitle(titleText)
            it.setDisplayHomeAsUpEnabled(showBackButton)
        }
    }
}
