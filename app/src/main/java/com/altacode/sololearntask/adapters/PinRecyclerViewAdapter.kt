package com.altacode.sololearntask

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.altacode.sololearntask.repository.models.FeedItem
import com.altacode.sololearntask.repository.models.PinDBO


import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.recycler_item_feed.view.*
import kotlinx.android.synthetic.main.recycler_item_pin.view.*

class PinRecyclerViewAdapter(
        var data: LinkedHashSet<PinDBO>,
        private val onItemCLick : OnItemClick) : RecyclerView.Adapter<PinRecyclerViewAdapter.ViewHolder>(){

    private val onClickListener = View.OnClickListener {

        onItemCLick.onItemClicked(it.tag as PinDBO,it.findViewById<SimpleDraweeView>(R.id.pin_image_view))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_pin, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = data.elementAt(position)
        holder.apply {
            image.setImageURI(item.fields.thumbnail)

            itemView.tag = item
            itemView.setOnClickListener(onClickListener)
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: SimpleDraweeView = view.pin_image_view
    }

    interface OnItemClick{
        fun onItemClicked(pinItem: PinDBO,view: View)
    }
}
