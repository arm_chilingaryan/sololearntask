package com.altacode.sololearntask

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.altacode.sololearntask.repository.models.FeedItem


import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.recycler_item_feed.view.*

class FeedRecyclerViewAdapter(
        var data: LinkedHashSet<FeedItem>,
        private val onItemCLick : OnItemClick) : RecyclerView.Adapter<FeedRecyclerViewAdapter.ViewHolder>(){

    var style: Style? = null
    private val onClickListener = View.OnClickListener {

        onItemCLick.onItemClicked(it.tag as FeedItem,it.findViewById<SimpleDraweeView>(R.id.image))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var layoutId = 0

        when (style) {
            Style.Grid -> layoutId = R.layout.recycler_item_feed_grid
            Style.Linear -> layoutId = R.layout.recycler_item_feed
        }

        val view = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = data.elementAt(position)
        holder.apply {
            image.setImageURI(item.fields.thumbnail)
            title.text = item.webTitle
            shortDescription.text = item.fields.trailText

            itemView.tag = item
            itemView.setOnClickListener(onClickListener)
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    enum class Style {
        Grid, Linear
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: SimpleDraweeView = view.image
        val title: TextView = view.title
        val shortDescription: TextView = view.short_description
    }

    interface OnItemClick{
        fun onItemClicked(feedItem: FeedItem,view: View)
    }
}
